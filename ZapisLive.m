% stworzenie api
Simulink.importExternalCTypes(which('kortex_wrapper_data.h'));
gen3Kinova = kortex;
gen3Kinova.ip_address = '82.145.75.250';
gen3Kinova.user = 'admin';
gen3Kinova.password = 'admin';
%% połączenie z robotem
isOk = gen3Kinova.CreateRobotApisWrapper();
if isOk
   disp('You are connected to the robot!'); 
else
   error('Failed to establish a valid connection!'); 
end

%% odczyt danych z robota

iterationTime = 1; %minimum number of seconds for each loop
for i = 1:10
    tic;                                %set clock
    [isOk,baseFb, actuatorFb, interconnectFb] = gen3Kinova.SendRefreshFeedback();        % <-- your code goes here
    while toc < iterationTime           %wait for the remaining iteration time
        % do nothing                    %leave this empty
    end
end
    
if isOk
%     disp('Base feedback');
%     disp(baseFb);
%     disp('Actuator feedback');
%     disp(actuatorFb);
%     disp('Gripper feedback');
%     disp(interconnectFb);
else
    error('Failed to acquire sensor data.'); 
end



%% zniszczenie api
isOk = gen3Kinova.DestroyRobotApisWrapper();
