% Note: For Linux, the adaptor name is libkinova_vision_imaq
vid2 = videoinput('kinova_vision_imaq', 2, 'MONO16');
vid2.FramesPerTrigger = 1;
src2 = getselectedsource(vid2);

% Optionally, view the adaptor version
imaqhwinfo(vid2)

% Optionally, change device properties
src2.CloseConnectionOnStop = 'Enabled';
src2.Ipv4Address = '82.145.75.250';
src2.ResetROIOnResolutionChange = 'Disabled';

% Optionally, change the Region of Interest
vid2.ROIPosition = [0 0 480 270];

im = preview(vid2);
ax = im.Parent;
im.CDataMapping = 'scaled';
colormap(ax, hot);
ax.CLimMode = 'auto';
ax.CLim = [0 20];
% closepreview(vid2);
% delete(vid2);