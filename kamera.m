vid1 = videoinput('kinova_vision_imaq', 1, 'RGB24');
vid1.FramesPerTrigger = 1;
src1 = getselectedsource(vid1);

% Optionally, view the adaptor version
imaqhwinfo(vid1)

% Optionally, change device properties
src1.CloseConnectionOnStop = 'Enabled';
src1.Ipv4Address = '82.145.75.250';
src1.ResetROIOnResolutionChange = 'Disabled';

% Optionally, change the Region of Interest
vid1.ROIPosition = [0 0 1280 720];

preview(vid1);
% closepreview(vid1);
% delete(vid1);